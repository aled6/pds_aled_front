import { Objects } from './objects'; 

export class AlarmClock {
    idAlarmClock : Number; 
    alarm : String;
    radioHrz : Number;
    radioStatus : Boolean;
    alarmStatus : Boolean;
    time : String;
    alarmUsine : String;
    radioHrzUsine : Number;
    radioStatusUsine : Boolean;
    alarmStatusUsine : Boolean;
    object : Objects;
}