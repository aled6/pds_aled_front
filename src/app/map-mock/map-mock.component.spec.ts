import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapMockComponent } from './map-mock.component';

describe('MapMockComponent', () => {
  let component: MapMockComponent;
  let fixture: ComponentFixture<MapMockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapMockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapMockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
